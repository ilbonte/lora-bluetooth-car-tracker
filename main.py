import _thread
import time
import pycom
import struct
import socket
import ubinascii
from network import LoRa
from network import Bluetooth

SYNC_INTERVAL_IN_SECONDS = 6

bluetooth = Bluetooth()
bluetooth.start_scan(-1)    # start scanning with no timeout

pycom.heartbeat(False) # Initialise built-in LED

# Initialise LoRa in LORAWAN mode
lora = LoRa(mode=LoRa.LORAWAN, device_class=LoRa.CLASS_C)

# Create an ABP authentication
# DEV ADDR=006a6ad1
# NWKSKEY=2cc23b080c141dc4abffb0fef7f7929c
# APPSKEY=7cc0a535f24adacb8095e1b070ea36ed
dev_addr = struct.unpack(">l", ubinascii.unhexlify('006a6ad1'))[0] # your device address here
nwk_swkey = ubinascii.unhexlify('2cc23b080c141dc4abffb0fef7f7929c') # your network session key here
app_swkey = ubinascii.unhexlify('7cc0a535f24adacb8095e1b070ea36ed') # your application session key here

# Join the network using ABP (Activation By Personalisation)
lora.join(activation=LoRa.ABP, auth=(dev_addr, nwk_swkey, app_swkey))

# Remove all the non-default channels
for i in range(3, 16):
    lora.remove_channel(i)

# Set the 3 default channels to the same frequency
lora.add_channel(0, frequency=868100000, dr_min=0, dr_max=5)
lora.add_channel(1, frequency=868100000, dr_min=0, dr_max=5)
lora.add_channel(2, frequency=868100000, dr_min=0, dr_max=5)

def to_ascii(hex_string):
    return ubinascii.hexlify(hex_string)

def extract_device_info(raw_info):
    # print(raw_info)
    return {
        'mac': to_ascii(raw_info.mac),
        'rssi': raw_info.rssi,
        'data': to_ascii(raw_info.data),
        'addr_type': raw_info.addr_type,
        'adv_type': raw_info.adv_type,
        'ts': time.time()
    }

def get_nearby_devices_info():
    bluetooth_devices = bluetooth.get_advertisements()
    return [extract_device_info(device)
            for device in bluetooth_devices]

def upload_to_lora(pkt):
    print("UPLOADING...")
    pycom.rgbled(0x000033)

    s = socket.socket(socket.AF_LORA, socket.SOCK_RAW) # create a LoRa socket
    s.setsockopt(socket.SOL_LORA, socket.SO_DR, 5) # set the LoRaWAN data rate
    s.setblocking(False) # make the socket non-blocking

    s.send(pkt)
    print(pkt + " sent")
    time.sleep(1)
    pycom.rgbled(0x000000)

    # downlink receiving
    rx, port = s.recvfrom(4096)
    if rx:
        print('Received: {}, on port: {}'.format(rx, port))

    s.close() # close the LoRa socket


# uplink sending and downlink receiving
i = 0;
last_upload_to_server = time.time()
to_be_uploaded = {}

def append_for_mac(mac_addr, data):
    if mac_addr not in to_be_uploaded:
        to_be_uploaded[mac_addr] = []
    to_be_uploaded[mac_addr].append(data)

while(True):
    i += 1
    # uplink sending
    pkt = b'PKT #' + str(i)

    devices_info = get_nearby_devices_info()
    for device_info in devices_info:
        append_for_mac(device_info['mac'], device_info)

    should_upload = (time.time() - last_upload_to_server) >= SYNC_INTERVAL_IN_SECONDS
    if should_upload:
        _thread.start_new_thread(upload_to_lora, [pkt])
        last_upload_to_server = time.time()

